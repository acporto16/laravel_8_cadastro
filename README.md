# laravel_8_cadastro

Parte do curso de Laravel 5.8 (udemy) implementado em Laravel 8 e PHP 8. Este projeto possui apenas 2 telas de CRUD (Produtos e Categorias). O projeto faz uso do modelo MVC, ORM com Eloquent, Bootstrap, componentes visuais, templates.